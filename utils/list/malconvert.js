import pako from 'pako'

import getFileBuffer from '~/utils/getfilebuffer.js'
import getFileText from '~/utils/getfiletext.js'

function tagValue(el, tag) {
  try {
    return el.getElementsByTagName(tag)[0].childNodes[0].nodeValue;
  } catch (err) {
    try {
      el.getElementsByTagName(tag)[0].childNodes[0]; // some elements have no nodeValue, so just return ""
      return ""
    } catch (err) {
      console.log("Element ignored: ", el, " ", tag)
      return ""
    }
  }
}

function malDateToUTC(date) {
  try {
    return ((new Date(date)).toISOString()) || null
  } catch (err) {
    return null
  }

}

function echoParse(value) {
  return value
}

const status_options = {
  "plan to watch": "planned",
  "plan to read": "planned",
  "completed": "completed",
  "watching": "current",
  "reading": "current",
  "dropped": "dropped",
  "on-hold": "on-hold"
}

function parseStatus(status) {
  return status_options[status.toLowerCase()]
}

const parseScore = (score => parseInt(score) == 0 ? null : parseInt(score))

const anime_entry = {
  mal_id: { name: "series_animedb_id", f: echoParse },
  progress: { name: "my_watched_episodes", f: parseInt },
  status: { name: "my_status", f: parseStatus },
  score: { name: "my_score", f: parseScore },
  start_date: { name: "my_start_date", f: malDateToUTC },
  end_date: { name: "my_finish_date", f: malDateToUTC },
  repetitions: { name: "my_times_watched", f: parseInt },
  comments: { name: "my_comments", f: echoParse }
}

const manga_entry = {
  mal_id: { name: "manga_mangadb_id", f: echoParse },
  progress: { name: "my_read_chapters", f: parseInt },
  status: { name: "my_status", f: parseStatus },
  score: { name: "my_score", f: parseScore },
  start_date: { name: "my_start_date", f: malDateToUTC },
  end_date: { name: "my_finish_date", f: malDateToUTC },
  repetitions: { name: "my_times_read", f: parseInt },
  comments: { name: "my_comments", f: echoParse },
  volumes: { name: "my_read_volumes", f: parseInt }
}
function convertMALEntry(mal_entry, type) {
  const model = (type === "anime" ? anime_entry : manga_entry)
  var entry = {}
  Object.entries(model).forEach(
    ([property, pmodel]) => {
      entry[property] = pmodel.f(tagValue(mal_entry, pmodel.name))
    }
  );
  var _id = entry.mal_id
  delete entry.mal_id
  return { _id, entry }
}

export default async function convertMAL(file) {
  var extracted
  try {
    const file_buffer = await getFileBuffer(file)
    var extracted = pako.inflate(file_buffer, { to: 'string' });
  } catch (err) {
    var extracted = await getFileText(file)
  }

  const xml_doc = (new DOMParser()).parseFromString(extracted, "text/xml");
  const xml_coll = {
    anime: xml_doc.getElementsByTagName("anime"),
    manga: xml_doc.getElementsByTagName("manga")
  }
  const type = xml_coll.anime.length ? "anime" : "manga"
  const list = Array.prototype.slice.call(xml_coll[type], 0)

  return { [type]: list.map(entry => convertMALEntry(entry, type)) }
}
