export function ensureArray(object) {
  if (Array.isArray(object)) {
    return object
  } else {
    return [object]
  }
}
export function ensureNotArray(object) {
  if (Array.isArray(object)) {
    if (object.length > 0) {
      return object[0]
    } else {
      return null
    }
  } else {
    return object
  }
}
