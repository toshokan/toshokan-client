export default function season(start_date) {
  if (!start_date) {
    return null
  }

  const month = new Date(start_date).getMonth()
  if (month >= 9) {
    return "Fall"
  } else if (month >= 6) {
    return "Summer"
  } else if (month >= 3) {
    return "Spring"
  } else {
    return "Winter"
  }

}
