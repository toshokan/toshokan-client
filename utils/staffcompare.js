const roleOrdering = ["Director", "Original Creator", "Co-Director", "Assistant Director", "Chief Animation Director", "Script", "Animation Director", "Art Director", "Executive Producer", "Chief Producer", "Producer", "Sound Director", "Storyboard", "Background Art", "Episode Director", "Character Design", "Music","Original Character Design", "Key Animation", "Theme Song Arrangement", "Director of Photography", "In-Between Animation", "Special Effects", "Production Assistant"]

export function staffcompare(a, b, isAsc){
    const a_index = roleOrdering.indexOf(a)
    const b_index = roleOrdering.indexOf(b)
    if(a_index == -1 && b_index == -1){
	return (isAsc ? 1 : -1) * a.localeCompare(b)
    }
    if(a_index == b_index){
	return 0
    }
    return (isAsc ? 1 : -1) * (a_index < b_index ? 1 : -1)
}
export function highestrole(roles){
    // Note for this function, lower is better. This is because of how sorting in Javascript works.
    var highest = 1000
    for(var role in roles){
	var place = roleOrdering.indexOf(roles[role])
	if(place != -1){
	    highest = Math.min(highest, roleOrdering.indexOf(roles[role]))
	}
    }
    return highest
}
