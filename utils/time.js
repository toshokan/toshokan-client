export function oneHourFromNow() {
  var now = new Date();
  var later = new Date();
  later.setMinutes(now.getMinutes() + 50); // it's actually 50 minutes because yes.
  return later
}

export function never() {
  return (new Date(2147483647000))
}
