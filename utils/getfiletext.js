export default function getFileText(inputFile) {
  const temporaryFileReader = new FileReader();

  return new Promise((resolve, reject) => {
    temporaryFileReader.onerror = () => {
      temporaryFileReader.abort();
      reject(new DOMException("Problem parsing input file."));
    };

    temporaryFileReader.onload = (event) => {
      resolve(event.target.result);
    };
    temporaryFileReader.readAsText(inputFile);
  });
}
