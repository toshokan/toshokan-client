var max = 1628469
var factor = 1000
export function popularity_from_level(level){
  return level/factor*max
}
export function level_from_popularity(pop){
  return Math.round((pop/max)*factor)
}
