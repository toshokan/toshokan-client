export default function ({ store, redirect, route }) {
  store.commit('setAuthorization', "auth")
  if (store.getters.isAuthenticated && !store.getters.isRegistered) {
    redirect('/complete-registration')
  }
}
