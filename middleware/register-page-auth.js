export default function ({ store, redirect, route }) {
  if (!store.getters.isAuthenticated || store.getters.isRegistered) {
    redirect('/')
  }
}
