export default ({ $axios, store, app, redirect }) => {
  if (store.getters.isRegistered) {
    $axios.setToken(store.getters.activeUser.token, 'Bearer')
  }

  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    console.log("Axios Error "+code+": "+error)
  })
}
// TODO: stop axios from sending cookies with requests
