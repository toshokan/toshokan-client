import { Auth } from '~/plugins/fireinit.js'
import * as time from '~/utils/time.js'
export default context => {
  const { $axios, nchan, store, app, redirect, route } = context
  return new Promise((resolve, reject) => {
    $axios.interceptors.request.use(function (config) {
      if (!Auth.currentUser) {
        return config
      }
      return Auth.currentUser.getIdTokenResult().then(idTokenResult => {
        let idToken = idTokenResult.token
        $axios.setToken(idToken, 'Bearer')
        config.headers.Authorization = `Bearer ${idToken}`;
        app.$cookies.set('token', idToken, { path: '/', expires: new Date(idTokenResult.expirationTime) })
        return Promise.resolve(config)
      })
    });
    app.$nchan.sub.on('disconnect', function (evt) {
      try {
        Auth.currentUser.getIdTokenResult().then(idTokenResult => {
          let idToken = idTokenResult.token
          app.$cookies.set('token', idToken, { path: '/', expires: new Date(idTokenResult.expirationTime) })
        })
      } catch (e) {
        throw "User does not exist. Backing off event source reconnection."
      }
    });
    Auth.onIdTokenChanged(async function(user) {
      if (user) {
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var uid = user.uid;
        var phoneNumber = user.phoneNumber;
        var providerData = user.providerData;
        var refreshToken = user.refreshToken
        var idTokenResult = await user.getIdTokenResult();
        var idToken = idTokenResult.token;
        $axios.setToken(idToken, 'Bearer')

        await store.dispatch('autoSignIn', {
          email: email,
          email_verified: emailVerified,
          uid: uid,
          providerData: providerData,
          token: idToken,
          token_expire: idTokenResult.expirationTime,
          photo_url: photoURL,
          refresh_token: refreshToken
        })
        // cookies to track if user is registered
        if (app.$cookies.get("registered_" + uid)) { // cookie "registered_UID" is set, we know user is registered.
          await store.dispatch('updateRegStatus', true)
        } else { // no registered_UID cookie present, we're not sure if the user is registered.
          // test if user is registered
          try {
            var response = await $axios.$post("/account/registered", { token: idToken })
            if (response.registered) {
              app.$cookies.set("registered_" + uid, true, { path: '/', "expires": time.never() }) // expire never
              await store.dispatch('updateRegStatus', true)
            } else { // user is not registered after all
              redirect('/complete-registration')
            }
          } catch (err) {
            console.log("Error retrieving registration status: " + err)
          }
        }
        if (store.getters.isRegistered) {
          app.$nchan.sub.start();
          console.log("Connecting to Event Source.")
        }
      }
      return resolve()
    })
  })
}
