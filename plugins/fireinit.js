import firebase from 'firebase/app'
import 'firebase/auth'

// Initialize Firebase
export const config = {
  apiKey: "AIzaSyA-YudMTW1AwSXopqnwarE70xdE2UvCMQc",
  authDomain: "toshokan-221318.firebaseapp.com",
  databaseURL: "https://toshokan-221318.firebaseio.com",
  projectId: "toshokan-221318",
  storageBucket: "toshokan-221318.appspot.com",
  messagingSenderId: "693451050235"
}
!firebase.apps.length ? firebase.initializeApp(config) : firebase.app()
export const Auth = firebase.auth()
export default firebase


