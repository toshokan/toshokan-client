import Vue from 'vue'
function cloneObject(obj) {
  var clone = {};
  for (var i in obj) {
    if (obj[i] != null && typeof (obj[i]) == "object")
      clone[i] = cloneObject(obj[i]);
    else
      clone[i] = obj[i];
  }
  return clone;
}
async function processListEvent(store, event) {
  await setMissingData(store, event)
  store.commit("list/applyEvents", [event])
  store.commit("list/setLastEvent", event)
}
async function setMissingData(store, event) {
  var store_data = store.getters["list/data"]
  var data = {}
  Object.assign(data, cloneObject(store_data))
  if (event.operation === "insert") {
      var r = await store.$axios.$get(event.library_ref.href + "?min=true")
      var type = event.library_ref.type
      // console.log(type)
      // console.log(data)
      var _id = event.library_ref.href.split("/")[2]
      data[type][_id] = r
      await store.commit("list/updateData", data)
  }
}

export default ({ $axios, store, app, redirect, route }, inject) => {

  if (process.browser) {
    let NchanSubscriber = require("nchan");
    var opt = {
      subscriber: 'websocket',
      reconnect: 'persist',
      shared: true
    }
    var sub = new NchanSubscriber("/event/sub", opt)
    sub.on('connect', function (evt) {
      console.log("Connected to Accelerator Event Source.")
    });
    sub.on("message", function (message, message_metadata) {
      let event = JSON.parse(message)
      if (event["ns"] === "list") {
        processListEvent(store, event)
      }
    })
    console.log("EventSource setup complete.")
    inject("nchan", new Vue({ data: { sub: sub } }))
  }
}
