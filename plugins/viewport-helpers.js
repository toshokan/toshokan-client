import Vue from 'vue'

const viewports = ['mobile', 'tablet', 'desktop', 'widescreen', 'fullhd']
export default ( context, inject) => {
    function geq(vp, mq){
	    return viewports.indexOf(mq) >= viewports.indexOf(vp)
    }

  function leq(vp, mq){
	  return viewports.indexOf(mq) <= viewports.indexOf(vp)
  }


  inject("viewport", new Vue({data: {geq, leq}}))

}
