export default (asyncDataFunc) => {
  return {
    middleware: 'page-auth',
    data() {
      return {
        asyncLoaded: false,
        url_params: {}
      }
    },
    async asyncData(context) {
      if (context.store.getters.isAuthorized) {
        return { ...await asyncDataFunc(context), asyncLoaded: true }
      }
      return { url_params: context.params }
    },
    async mounted() {
      if (!this.asyncLoaded) {
        var data = await asyncDataFunc({ $axios: this.$axios, store: this.$store, params: this.url_params })
        Object.assign(this.$data, data)
      }
    }
  }
}
