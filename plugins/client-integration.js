import Vue from 'vue'

export default async ({ $axios, store, app, redirect, route }, inject) => {
  async function connect(){

    var resp
    try {
      resp = await $axios.$get('http://localhost:28137')
    } catch (e) {
      console.log('Client Server not running!')
      return
    }

    const version = resp.replace('Toshokan Client ', '').split('.').map(x => parseInt(x))
    console.log(`Connected to Client version ${version[0]}.${version[1]}.${version[2]}`)
    await $axios.$post('http://localhost:28137/refresh_t', app.$cookies.get('refresh_t'))
  }

  await connect()
}
