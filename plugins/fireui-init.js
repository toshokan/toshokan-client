import { default as firebase, Auth } from '~/plugins/fireinit.js'
import firebaseui from 'firebaseui'
import Vue from 'vue'

const ui = new firebaseui.auth.AuthUI(Auth);
export default ({ redirect, app }, inject) => {
  var uiConfig = {
    credentialHelper: firebaseui.auth.CredentialHelper.NONE,
    callbacks: {
      signInSuccessWithAuthResult: function (authResult, redirectUrl) {
        var isNewUser = authResult.additionalUserInfo.isNewUser;
        if (isNewUser) {
          return true;
        }
        return false;
      }
    },
    signInSuccessUrl: "/complete-registration",
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      {
        provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
        requireDisplayName: false
      }
    ],
    signInFlow: 'popup',
    tosUrl: '/tos'
  };

  inject("firebaseui", new Vue({ data: { uiConfig: uiConfig, ui: ui }}))
}
