import mergeDeep from "~/utils/mergedeep.js"
export const state = () => ({
  last_event: null,
  fetched_full: {anime: false, manga: false, novel: false, vn: false},
  anime: {},
  manga: {},
  novel: {},
  vn: {},
  data: { anime: {}, manga: {}, novel: {}, vn: {}}
})

export const getters = {
  anime(state, getters) {
    return state.anime
  },
  manga(state, getters) {
    return state.manga
  },
  novel(state, getters) {
    return state.novel
  },
  vn(state, getters) {
    return state.vn
  },
  data(state, getters) {
    return state.data
  },
  list: (state, getters) => (type) => {
    return Object.keys(state[type]).map(function (_id) {
      if (state[type][_id]) {
        return { _id, entry: state[type][_id] };
      }
    })
  },
  expire(state, getters) {
    return state.expire
  }
}

export const mutations = {
  setList(state, { type, list, data }) {
    var list_obj = list.reduce(function (map, obj) {
      map[obj._id] = obj.entry;
      return map;
    }, {});
    state[type] = list_obj
    state.data = { ...state.data, [type]: { ...state.data[type], ...data } }
    state.fetched_full[type] = true
  },
  setEntry(state, { type, _id, entry }) {
    state[type] = { ...state[type], [_id]: entry}
  },
  applyEvents(state, events) {
    for (var ev in events) {
      var event = events[ev]
      var type = event.library_ref.type
      var _id = event.library_ref.href.split("/")[2]
      state[type] = {
        ...state[type], [_id]: { ...state[type][_id], ...event.fields }}
    }
  },
  setLastEvent(state, { _id }) {
    state.last_event = _id
  },
  updateData(state, data) {
    state.data = { ...mergeDeep(state.data, data) }
  }
}

export const actions = {
  async fetchList({ commit }, payload) {
    var r = await this.$axios.$get(`/list/${payload}`)
    commit('setList', { type: payload, list: r.list, data: r.data })
  },
  async fetchEntry({ commit }, { type, _id }) {
    var r = await this.$axios.$get(`/list/${type}/${_id}`)
    commit('setEntry', { type, _id, entry: r })
  },
  async ensureList({ commit, state, dispatch }, payload) {
    var type = payload
    if (!state.fetched_full[type]) { // the list has not been fetched at least once yet

      console.log(`Fetching ${type} list...`)
      await dispatch('fetchList', type)
    }
  },
  async ensureLists({ commit, dispatch }, payload) {
    for (list in payload) {
      await dispatch('ensureList', payload[list])
    }
  },
  async ensureEntry({ commit, state, dispatch }, { type, _id }) {
    if ((state[type][_id] === undefined) && (!state.fetched_full[type])) {
      console.log("Entry not found! Fetching...")
      await dispatch('fetchEntry', { type, _id })
    }
  }
}
