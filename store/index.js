import Vuex from 'vuex'
import Vue from 'vue'
import { Auth, config } from '~/plugins/fireinit.js'

import * as time from '~/utils/time.js'
import list_plugin from "~/store/list-plugin.js"

export const plugins = [list_plugin()]
export const state = () => ({
    user: null,
    authorization: null,
    loading: false
})

export const getters = {
  activeUser: (state, getters) => {
    return state.user
  },
  isAuthenticated: (state) => {
    return !!(state.user !== null && state.user !== undefined && state.user.token)
  },
  isRegistered: (state, getters) => {
    return !!(getters.isAuthenticated && state.user.registered)
  },
  isAuthorized: (state, getters) => {
    if (state.authorization === "auth") {
      return getters.isRegistered
    }
    return true
  },
  isLoading: (state, getters) => {
    return state.loading
  }
}

export const mutations = {
  setUser(state, payload) {
    var registered = false;
    if (state.user && state.user.registered) {
      registered = state.user.registered;
    }
    state.user = { ...payload, registered }
  },
  setToken(state, payload) {
    state.user = { ...state.user, token: payload }
  },
  setRefreshToken(state, payload) {
    state.user = { ...state.user, refresh_t: payload }
  },
  setRegistered(state, payload) {
    var registered = (payload ? true : false) // ensure registered is always true or false
    state.user = { ...state.user, registered }
  },
  setAuthorization(state, payload) {
    state.authorization = payload
  },
  setLoading(state, payload) {
    state.loading = payload
  }
}

export const actions = {
  async nuxtServerInit({ commit, dispatch }, { req }) {
    console.log("nuxtServerInit")
    await dispatch('setSSRInitialState')
  },
  async setSSRInitialState({ commit }) {
    var refresh_token = this.$cookies.get('refresh_t')
    config
    var token = this.$cookies.get('token')
    if (token) {
      commit('setToken', token)
    } else if (refresh_token) {
      try {
        var response = await this.$axios.$post(`https://securetoken.googleapis.com/v1/token?key=${config.apiKey}`, { grant_type: "refresh_token", refresh_token })
        commit('setRefreshToken', response.refresh_token)
        this.$cookies.set('refresh_t', response.refresh_token, { path: '/', expires: time.never() })
        commit('setToken', response.id_token)
        let expires_in_millisec = response.expires_in * 1000
        let expirationTime = (new Date()).getTime() + expires_in_millisec
        this.$cookies.set('token', response.access_token, { path: '/', expires: new Date(expirationTime) })
      } catch (err) {
        console.log("Error fetching idToken from Firebase: " + err)
      }
    }
    commit('setRegistered', !!this.$cookies.get('registered'))
  },
  autoSignIn({ commit }, payload) {
    commit('setUser', payload)
    this.$cookies.set('token', payload.token, { path: '/', expires: new Date(payload.token_expire) }) // expire in 50 minutes
    this.$cookies.set('refresh_t', payload.refresh_token, { path: '/', expires: time.never() })
  },
  updateRegStatus({ commit }, payload) {
    commit('setRegistered', payload)
    this.$cookies.set("registered", payload, { path: '/', expires: time.never() })
  },
  async signOut({ commit }) {
    this.$router.push('/')
    commit('setUser', null)
    await Auth.signOut()
    this.$cookies.remove("registered")
    this.$cookies.remove("token")
    this.$cookies.remove("refresh_t")
    this.$nchan.sub.stop()
  }
}
