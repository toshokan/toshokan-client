module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Toshokan.moe',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Toshokan: Anime Tracking for Humans' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', size: '16x16', href: '/favicon-16.ico' },
      { rel: 'icon', type: 'image/x-icon', size: '32x32', href: '/favicon-32.ico' },
      { rel: 'icon', type: 'image/x-icon', size: '64x64', href: '/favicon-64.ico' },
      //{ rel: 'stylesheet', href: 'https://www.gstatic.com/firebasejs/ui/3.2.0/firebase-ui-auth.css' },
      { rel: 'stylesheet', href: '//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css' },
    ],
    script: [
      {src: "https://polyfill.io/v3/polyfill.min.js?features=fetch%2CObject.entries%2CIntersectionObserver", body: true}
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    /*
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    }*/
  },
  modules: [
    'nuxt-material-design-icons',
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/moment',
    ['nuxt-mq', {
	      breakpoints: {
          mobile: 769,
          tablet: 1024,
          desktop: 1216,
          widescreen: 1408,
          fullhd: Infinity
        }
    }]
  ],
  css: [
    { src: '~/assets/css/main.scss', lang: 'scss' },
    { src: '~/assets/css/dark-theme.scss', lang:'scss' },
    { src: '~/node_modules/firebaseui/dist/firebaseui.css', lang: 'css' },
    { src: '~/node_modules/flag-icon-css/css/flag-icon.css', lang:'css' }
    //{ src: '~/assets/css/materialdesignicons.min.css', lang: 'css' },
  ],
  plugins: [
    { src: '~/plugins/buefy.js', ssr: true },
    { src: '~/plugins/vuelidate.js',ssr: false },
    { src: '~/plugins/fireinit.js', ssr: false },
    { src: '~/plugins/nchan.js', ssr: false },
    { src: '~/plugins/fireauth.js', ssr: false },
    { src: '~/plugins/viewport-helpers.js', ssr: false },
    { src: '~/plugins/fireui-init.js', ssr: false }, // TODO: move this into login_form for performance.
  ],

  router: {
    middleware: ['axios-init','router-default']
  },
  axios: {
    prefix: '/api/1/',
    port: 3000,
    host: "127.0.0.1",
    https: false,
    retry: true,
    credentials: false,
    proxyHeaders: false,
    proxy: true
  },
  proxy: {
    '/api/1/search': 'https://toshokan.moe/',
    '/api/1': getApiLocation(),
    '/event/sub': 'https://toshokan.moe/'
  }
}

function getApiLocation(){
    const fs = require('fs')

    if (fs.existsSync('.local-api')){
        return 'http://127.0.0.1:8128'
    } else {
        return 'https://toshokan.moe/'
    }
}
